import {Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import {ICourse} from './types';
import axios from 'axios';
import * as https from 'https';
import * as cheerio from 'cheerio';
import {CourseSeason, EducationLevel} from "../../common/types";


@Injectable()
export class CourseService {
    async getCourse(code : number) : Promise<ICourse>{
        const httpsAgent = new https.Agent({rejectUnauthorized: false});
        const response = await axios.get(`https://my.ukma.edu.ua/course/${code}`, {httpsAgent})
            .catch(err => {
                if (err.response.status === 404) throw new NotFoundException("Page not found");
                throw new InternalServerErrorException("Server is unreachable");
            });
        const data = await response.data;
        return this.parseCourse(data);
    }
    private parseCourse(data : string) : ICourse {
        const $ = cheerio.load(data);
        const code: number = +$(this.getFromTable(1, 1, 1)).text();
        const name: string = $('div.container > ul.breadcrumb > li.active').text();
        const facultyName: string = $(this.getFromTable(1, 3, 1)).text();
        const departmentName: string = $(this.getFromTable(1, 4, 1)).text();
        // const level: EducationLevel | undefined = this.parseLevel($(this.getFromTable(1, 4, 1)).text());
        const level: EducationLevel= this.parseLevel($(this.getFromTable(1, 4, 1)).text());
        const year: number = +$(this.getFromTable(1, 2, 1) + ` > span:nth-child(3)`).text().split(' ')[0];
        const seasons: CourseSeason[] = [];
        $(`#w0 > table > tbody:nth-child(2)`).each((i, el) => {
            const items = $(el).find('th').text().split('\t');
            items.forEach(item => {
                if(item === 'Осінь') {
                    seasons.push(CourseSeason.AUTUMN);
                }
                else if(item === 'Літо') {
                    seasons.push(CourseSeason.SUMMER);
                }
                else if(item === 'Весна') {
                    seasons.push(CourseSeason.SPRING);
                }
            });
        });

        // ?
        const description: string = $(this.getFromTable(1, 7, 1) + ` > div`).text().trim();
        const creditsAmount: number = +$(this.getFromTable(1, 2, 1) + ` > span:nth-child(1)`).text().split(' ')[0];
        const hoursAmount: number = +$(this.getFromTable(1, 2, 1) + ` > span:nth-child(2)`).text().split(' ')[0];
        const teacherName: string = $(this.getFromTable(1, 7, 1)).text().trim();


        return {
            code: code,
            name: name,
            description: description === '' ? undefined : description,
            facultyName: facultyName,
            departmentName: departmentName,
            level: level,
            year: year == 1 ? 1 : year == 2 ? 2 : year == 3 ? 3 : 4,
            seasons: seasons,
            creditsAmount: creditsAmount === 0 ? undefined : creditsAmount,
            hoursAmount: hoursAmount === 0 ? undefined : hoursAmount,
            teacherName: teacherName === '' ? undefined : teacherName,
        };
    }
    private getFromTable(tbodyN: number, trN: number, tdN: number) : string {
        return `#w0 > table > tbody:nth-child(${tbodyN}) > tr:nth-child(${trN}) > td`;
    }
    private parseLevel(uaName: string) : EducationLevel /*| undefined */{
        if(uaName === 'Магістр') return EducationLevel.MASTER;
        // if(uaName === 'Бакалавр') return EducationLevel.BACHELOR;
        // return undefined;
        return EducationLevel.BACHELOR;
    }
}
