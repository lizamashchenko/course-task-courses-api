import {Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import {IScheduleItem} from './types';
import * as https from 'https';
import axios from 'axios';
import * as cheerio from 'cheerio';
import {CourseSeason, EducationLevel} from '../../common/types';

@Injectable()
export class ScheduleService {

    async getSchedule(year: number, season: 'spring' | 'summer' | 'autumn') : Promise<IScheduleItem[]>{
        const httpsAgent = new https.Agent({rejectUnauthorized: false});
        const yearN = season === 'spring' || season === 'summer' ? year - 1 : year;
        const seasonN = season === 'autumn' ? 1 : season == 'spring' ? 2 : 3;
        const courseSeason = seasonN == 1 ? CourseSeason.AUTUMN : seasonN == 2 ? CourseSeason.SPRING : CourseSeason.SUMMER;
        const response = await axios.get(`https://my.ukma.edu.ua/schedule/?year=${yearN}&season=${seasonN}`, {httpsAgent})
            .catch(err => {
                if (err.response.status === 404) throw new NotFoundException("Page not found");
                throw new InternalServerErrorException("Server is unreachable");
            });
        const data = await response.data;
        return this.parseSchedule(data, courseSeason);
    }
    private parseSchedule(data: string, courseSeason: CourseSeason) : IScheduleItem[] {
        const allSchedules: IScheduleItem[] = [];

        const $ = cheerio.load(data);

        $(`#schedule-accordion > div`).each((i, facultyPanel) => {
            const facultyName: string = ($(facultyPanel).find(`div`).eq(0)).text().trim();

            $(facultyPanel).find(`.panel-default`).each((i, yearPanel) => {
                const year: 1 | 2 | 3 | 4 = (+$(yearPanel).find(`.panel-heading > h4 > a`)
                    .text().trim().split(' ')[1]) as 1 | 2 | 3 | 4;
                const levelS = $(yearPanel).find(`.panel-heading > h4 > a`)
                    .text().trim().split(' ')[0];
                const level: EducationLevel = levelS == 'БП' ? EducationLevel.BACHELOR : EducationLevel.MASTER;

                $(yearPanel).find(` div:nth-child(2) > ul > li`).each((i, scheduleItem) => {
                    const url: string = $(scheduleItem).find(`div > a.href`).text();
                    const updatedAt: string = this.parseDate($(scheduleItem).find(`div > span`).text().trim().split(':'));
                    const specialityName: string = $(scheduleItem).find(`div > a:last-child`).text().trim().split(' ')[0];

                    allSchedules.push({
                        facultyName: facultyName,
                        specialityName: specialityName,
                        level: level,
                        year: year,
                        season: courseSeason,
                        url: url,
                        updatedAt: updatedAt,
                    })
                });
            });
        });

        return allSchedules;
    }
    private parseDate(date: string[]): string  {  // 'DD-MM-YYYY HH:mm:ss' -> 'YYYY-MM-DD HH:mm:ss'
        console.log(date);

        const dateDate = date[1].trim().split(' ')[0];
        console.log(dateDate);
        const dateTime = `${date[1].trim().split(' ')[1]}:${date[2]}:${date[3].substring(0, date[3].length - 1)}`;
        console.log(dateTime);

        const day = dateDate.split('.')[0];
        console.log(day);
        const month = dateDate.split('.')[1];
        console.log(month);
        const year = dateDate.split('.')[2];
        console.log(year);

        return `${year}-${month}-${day} ${dateTime}`
    }
}
