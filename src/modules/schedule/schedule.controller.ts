import {BadRequestException, Controller, Get, Param} from '@nestjs/common';
import { ScheduleService } from './schedule.service';

@Controller('schedule')
export class ScheduleController {
  constructor(
    protected readonly service: ScheduleService,
  ) {}

  @Get(':year/:season')
  public async getSchedule(@Param('year') year: number, @Param('season') season : string): Promise<unknown> {
    if(season !== 'autumn' && season !== 'spring' && season !== 'summer')
      throw new BadRequestException('Season is entered incorrectly');
    return this.service.getSchedule(year, season as 'autumn' | 'spring' | 'summer');
  }
}
